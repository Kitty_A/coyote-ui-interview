import React from 'react';
import EditableInput from './components/editable-input/index.js'

import './App.css';

function App() {

  return (
    <div className="App">
        <span>Here is some regular text and this is <EditableInput /></span>
    </div>
  );
}

export default App;

