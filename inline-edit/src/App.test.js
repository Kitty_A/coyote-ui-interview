import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const startingText = getByText(/Here is some regular text and this is/i);
  expect(startingText).toBeInTheDocument();
});
