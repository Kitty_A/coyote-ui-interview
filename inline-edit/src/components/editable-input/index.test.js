import React from "react";

import { render, fireEvent } from "@testing-library/react";
import { screen } from "@testing-library/dom";

import EditableInput from "./index";

const setup = () => {
  const utils = render(<EditableInput />);
  const input = utils.getByTestId("editable-input");
  return {
    input,
    ...utils,
  };
};

test("It should change text when edited", () => {
  const { input, unmount } = setup();
  expect(input.value).toBe("editable text");
  fireEvent.change(input, { target: { value: "updated text" } });
  expect(input.value).toBe("updated text");
  unmount();
});

test("It should show a loading icon when new text is entered", () => {
  const { input, unmount } = setup();
  input.focus();
  fireEvent.change(input, { target: { value: "different text" } });
  input.blur();
  const loadingIcon = screen.getByTestId("loading-icon");
  expect(loadingIcon).toBeTruthy();
  unmount();
});
