function mockCheckInput(text) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (text === "hello world") {
        let message = "Oops! Something has gone terribly wrong!";
        reject(message);
      } else {
        resolve();
      }
    }, 1000);
  });
}

export default mockCheckInput;
