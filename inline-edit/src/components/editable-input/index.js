import React, { useState } from "react";
import { Loader, XCircle, CheckCircle } from "react-feather";
import mockCheckInput from "./mock.js";
import "./styles.css";

function EditableInput() {
  const [editableText, setEditableText] = useState("editable text");
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  function handleEditableText(e) {
    setEditableText(e.target.value);
    setError(false);
    setSuccess(false);
  }

  function handleSubmitText(e) {
    e.preventDefault();
    setLoading(true);
    mockCheckInput(editableText)
      .then(() => {
        setSuccess(true);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }

  return (
    <div className="editable-text-container">
      <form onSubmit={(e) => handleSubmitText(e)}>
        <input
          data-testid="editable-input"
          className="editableText"
          onBlur={(e) => handleSubmitText(e)}
          onChange={(e) => handleEditableText(e)}
          onClick={(e) => handleEditableText(e)}
          value={editableText}
        ></input>
        {error && <p className="text-error">{error}</p>}
      </form>
      {error && (
        <span className="icon-container">
          <XCircle color="tomato" width={16} height={16} />
        </span>
      )}
      {loading && (
        <span className="icon-container" data-testid="loading-icon">
          <Loader color="black" width={16} height={16} />
        </span>
      )}
      {success && (
        <span className="icon-container">
          <CheckCircle color="blue" width={16} height={16} />
        </span>
      )}
    </div>
  );
}

export default EditableInput;
